//controller.js

$(function () {
	console.log( "DOM ready!" );
	$('form').on( 'submit' , validarDatos );
});

function validarDatos ( event ) {
	var aceptarDatos=true;
	// 1.- Recuperar datos de controles del formulario
	// 2.1.- Si datos inválidos, mostrar mensajes de error
	// 2.2.- Si todos los datos son correctos, completar el envío
	var cadena= $('[name=fragment]').val();
	if (cadena.length <2 || cadena.length > 20) {
		aceptarDatos=false;
		$('#errFragment').text("Longitud de la cadena entre 2 y 20 caracteres");
	}

	return aceptarDatos;
}
