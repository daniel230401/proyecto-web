/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const validacionNumerica = function (cadena) {
    var num = "0123456789";
    var valido;
    for (var i = 0; i < cadena.length; i++)
    {
        valido = false;
        var j = 0;
        while (!valido && j < num.length)
        {
            if (num[j] == cadena[i])
            {
                valido = true;
            }
            j++;
        }
        if (!valido)
        {
            return false;
        }
    }
    return true;
}


$("#boton_register").mousedown(function () {
    //variables necesarias
    pass = $("#password_register").val();
    cpass = $("#cpassword_register").val();
    mail = $("#email_register").val();
    nick = $("#nick_register").val();
    name = $("#name_register").val();
    surname = $("#surname_register").val();
    phone = $("#phone_register").val();
    console.log(nick);
    var cadena = "<ul>";
    var fallos = false;

    //verifica nick
    if (nick == "")
    {
        fallos = true;
        $("#nick_register").css("border", "3px solid #dc3545");
        cadena += "<li>El nick esta vácio</li>";
    }

    //verifica nombre
    if (name == "")
    {
        fallos = true;
        $("#name_register").css("border", "3px solid #dc3545");
        cadena += "<li>El nombre esta vácio</li>";
    }

    //verifica apellidos
    if (surname == "")
    {
        fallos = true;
        $("#surname_register").css("border", "3px solid #dc3545");
        cadena += "<li>Los apellidos estan vácio</li>";
    }

    //verifica telefono
    if (phone == "")
    {
        fallos = true;
        $("#phone_register").css("border", "3px solid #dc3545");
        cadena += "<li>El teléfono esta vácio</li>";
    } else if (!validacionNumerica(phone))
    {
        fallos = true;
        $("#phone_register").css("border", "3px solid #dc3545");
        cadena += "<li>El teléfono debe ser numérico</li>";
    } else if (phone.length < 9)
    {
        fallos = true;
        $("#phone_register").css("border", "3px solid #dc3545");
        cadena += "<li>El teléfono debe tener una longitud de 9</li>";
    }

    //verifica email
    if (mail == "")
    {
        fallos = true;
        $("#email_register").css("border", "3px solid #dc3545");
        cadena += "<li>El email esta vácio</li>";
    } else {
        var arroba = false;
        var punto = false;
        for (let letra of mail)
        {
            if (letra == "@")
            {
                arroba = true;
            }
            if (arroba && letra == ".")
            {
                punto = true;
            }
        }
        if (!punto)
        {
            fallos = true;
            $("#email_register").css("border", "3px solid #dc3545");
            cadena += "<li>email no valido</li>";
        }
    }
    //verifica contraseñas
    if (pass != cpass)
    {
        fallos = true;
        cadena += "<li>Las contraseñas no coinciden</li>";
        $("#cpassword_register").css("border", "3px solid #dc3545");
        $("#password_register").css("border", "3px solid #dc3545");
    } else if (pass.length > 0 && pass.length < 5)
    {
        fallos = true;
        cadena += "<li>La contraseña debe tener logitud mayor de cinco</li>";
        $("#cpassword_register").css("border", "3px solid #dc3545");
        $("#password_register").css("border", "3px solid #dc3545");
    } else if (pass == "")
    {
        fallos = true;
        cadena += "<li>La contraseña esta vácia</li>";
        $("#cpassword_register").css("border", "3px solid #dc3545");
        $("#password_register").css("border", "3px solid #dc3545");
    }

    //mostrar el dev
    cadena += "</ul>";
    if (fallos == true)
    {
        $("#fallos").html(cadena);
        $("#fallos").show();
    } else
    {
        //restaura por si es la segunda vez que lo pulsa
        $("#fallos").hide();
        $("#email_register").css("border", "1px solid #ced4da");
        $("#cpassword_register").css("border", "1px solid #ced4da");
        $("#nick_register").css("border", "1px solid #ced4da");
        $("#name_register").css("border", "1px solid #ced4da");
        $("#surname_register").css("border", "1px solid #ced4da");
        $("#phone_register").css("border", "1px solid #ced4da");
        $("#password_register").css("border", "1px solid #ced4da");
    }
});
