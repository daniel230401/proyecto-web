<!doctype html>
<html lang="en">
    <head>
        <!-- Bootstrap core CSS -->
        <link href="<?= base_url("css/bootstrap.min.css") ?>" rel="stylesheet">

        <!-- Bootstrap Icons core CSS -->
        <link href="<?= base_url("css/font-awesome-4.7.0/css/font-awesome.min.css") ?>" rel="stylesheet">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="<?= base_url("pages/view") ?>" href="#">Ludoteca Ludovico</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a href="<?= base_url("pages/view") ?>" class="nav-link active" aria-current="page" href="#">Inicio</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url("material/show")?>" class="nav-link active" aria-current="page" href="#">Materiales</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url("activity/show")?>" class="nav-link active" aria-current="page" href="#">Actividades</a>
                            </li>
                            <li class="d-flex dropdown ">
                                <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle">
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a href="<?= base_url("user/preferencias")?>" class="dropdown-item" href="#">Perfil</a></li>
										<a href="<?= base_url("reserva/reserva_list")?>" class="dropdown-item">Mis materiales</a>
										<a href="<?= base_url("inscripcion/inscripcion_list")?>" class="dropdown-item">Mis actividades</a>
                                    <li><a class="dropdown-item" href="<?= base_url("user/cerrarSesion") ?>">Cerrar Sesión</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </header>
