<body>
</br>
<div class="container center-block">
	<?php
	form_open('activity/add_activity');
	echo form_open('activity/add_activity');
	?>
	<h1>Nueva Actividad</h1>
		<div class="form-outline mb-2 col-xs-5 col-md-4">
					<label class="form-label" for="actividad_nombre">Nombre</label>
			<input class="form-control" type="actividad_nombre" name="actividad_nombre" value="">

		</div>
		<div class="form-outline mb-2 col-xs-5 col-md-4">
					<label class="form-label" for="actividad_descripcion" >Descripción</label>
			<textarea class="form-control" type="text" name="actividad_descripcion" rows="10" cols="10" value=""></textarea>

		</div>
		<div class="form-outline mb-2 col-xs-5 col-md-4">
					<label class="form-label" for="actividad_plaza_total">Plazas totales</label>
			<input class="form-control" type="number" name="actividad_plaza_total" value="">

		</div>
		<div class="form-outline mb-2 col-xs-5 col-md-4">
					<label class="form-label" for="actividad_fecha_hora">Fecha y Hora</label>
			<input class="form-control col-auto" type="date" name="actividad_fecha_hora" value="">

		</div>
		<button type="submit" class="btn btn-primary btn-lg col-auto">Añadir</button>

	</form>
</div>
</body>