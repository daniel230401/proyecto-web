<?php ?>
<body>
    Nota: evento fallito de js
    <div class="container">

        <i class="far fa-bell"></i>


        <div class="signup-form-container center-block" style="width:50%">

            <!-- form start -->

            <?=
            form_open('user/add_register');
            ?>

            <div class="form-header">
                <h3 class="form-title"><i class="fa fa-user"></i> Registrarse</h3>

                <div class="pull-right">
                    <h3 class="form-title"><span class="glyphicon glyphicon-pencil"></span></h3>
                </div>

            </div>

            <div id="fallos" class="alert alert-danger ocultar" role="alert" style="display:none;"></div>

            <div class="form-body">

                <div class="form-group">

                    <div class="input-group">
                        <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                        <input name="nick_register" id="nick_register" type="text" class="form-control" placeholder="Nick">
                    </div>

                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                        <input name="name_register" id="name_register" type="text" class="form-control" placeholder="Nombre">
                    </div>

                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                        <input name="surname_register" id="surname_register" type="text" class="form-control" placeholder="Apellidos">
                    </div>

                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                        <input name="phone_register" id="phone_register" type="phone" class="form-control" placeholder="Teléfono">
                    </div>

                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                        <input name="email_register" id="email_register" type="email" class="form-control" placeholder="Email">
                    </div> 

                </div>

                <div class="row">

                    <div class="form-group col-lg-6">
                        <div class="input-group">
                            <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                            <input name="password_register" id="password_register" type="password" class="form-control" placeholder="Password">
                            <div name="eval" id="eval"></div>
                        </div>  

                    </div>

                    <div class="form-group col-lg-6">
                        <div class="input-group">
                            <span class="input-group-text" id=""><i class="fa fa-user-o" aria-hidden="true"></i></span>
                            <input name="cpassword_register" id="cpassword_register" type="password" class="form-control" placeholder="Retype Password">
                        </div>  

                    </div>

                </div>


            </div>

            <div class="form-footer">
                <button type="submit" class="btn btn-primary mb-2" id="boton_register">
                    <i class="fa fa-user-o" aria-hidden="true"></i> Registrame!
                </button>
            </div>


            </form>

        </div>

    </div>

