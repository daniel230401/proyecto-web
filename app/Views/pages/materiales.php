<!DOCTYPE HTML>
<head>
</head>

<body>
</br>
<h1>Listado de materiales</h1>
</br>
<?php

if(session('rol')=="ADMIN"){
	echo '<a href="new_material">';
		echo '<input type="button" class="btn btn-success" value="Añadir material">';
	echo '</a>';	
}

echo "<table class='table table-striped'>";
echo "<tr>";
echo "  <th  scope='col'> Nombre  </th>";
echo "  <th scope='col'> Descripción </th>";
echo "  <th scope='col'> Cantidad </th>";
echo "  <th scope='col'></th>";
if(sizeof($materiales)>0){
	foreach($materiales as $row){
		echo "<tr>";
		$material = $row->id;
			echo '<td>' .$row->nombre. '</td>';
			echo "<td>" .$row->descripcion. "</td>";
			if(null!=session('nick')){ //Sesión iniciada 
				echo "<td>";
					if($row->cantidad!="0"){
					echo '<form action='.base_url("reserva/reserva").'>';
						echo '<input type="text" style="display:none" name="id" value="'.$material.'">';
						echo '<button class="btn btn-success" type="submit">'.$row->cantidad.'/'.$row->cantidad_total.'</button>';
					}else{
						echo "<p class='text-danger'>AGOTADO</p>";
					}
				echo "</td>";
					echo '</form>';
					if(session('rol')=="ADMIN"){
						echo "<td>";
						
							echo '<form action='.base_url("material/borrar").'>';
								echo '<input type="text" style="display:none" name="id" value="'.$material.'">';
								echo '<button class="btn btn-danger" type="submit">Borrar</button>';
							echo "</form>";
						
							echo '<form action='.base_url("material/change_material").'>';
								echo '<input type="text" style="display:none" name="id" value="'.$material.'">';
								echo '<button class="btn btn-primary" type="submit">Modificar</button>';
							echo "</form>";
						echo "</td>";
					}
				
			}	
		echo "</tr>";
		echo "<br/>";
	}
}else{
	echo "No existen materiales en la base de datos.";
}
echo "</tr>";
echo "</table>";

?>
