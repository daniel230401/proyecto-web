<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container bootstrap snippets bootdeys">
<div class="row">
  <div class="col-xs-12 col-sm-9">
            <?php
            //form_open('user/change_preferencias');
			echo '<form action='.base_url('user/change_preferencias').' enctype="multipart/form-data">';
            ?>
			</br>
			</br>
      <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title">Información de usuario</h4>
        </div>
			<?php
            
			if(session()->getFlashdata('fallos_preferencias')){
			echo '<div id="fallos" class="alert alert-danger ocultar" role="alert" >'.session()->getFlashdata('fallos_preferencias').'</div>';
			}
			if(session()->getFlashdata('cambiado_preferencias')){
			echo '<div id="fallos" class="alert alert-success ocultar" role="alert" >Sus cambios se han hecho correctamente</div>';
			}
			?>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label" >Nombre</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preferencias_nombre" name="preferencias_nombre">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Apellidos</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preferencias_apellidos" name="preferencias_apellidos">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Teléfono</label>
            <div class="col-sm-10">
              <input type="phone" class="form-control" id="preferencias_telefono" name="preferencias_telefono">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Correo electronico</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preferencias_email" name="preferencias_email">
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title">Seguridad</h4>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label" >Contraseña actual</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="preferencias_pass_actual" name="preferencias_pass_actual">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Contraseña nueva</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="preferencias_pass_new" name="preferencias_pass_new">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
				<!-- Button trigger modal -->
				<button type="submit" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
				  Actualizar
				</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

