<div class="mb-2 col-xs-5 col-md-7"">
            <?php
			
			$id=session()->getFlashdata('id');
			$model = new \App\Models\MaterialModel();
			$valores=$model->authenticate($id,'id');
			echo '<form action='.base_url("material/change").'>';
            ?>
      <div class="panel panel-default">
        <div class="panel-heading">
		</br>
        <h4 class="panel-title">Información del material</h4>
        </div>
		<?php
			if(session()->getFlashdata('fallo_cantidad')){
				echo '<div class="alert alert-danger" role="alert">
				La cantidad actual no puede ser mayor que la cantidad total
				</div>';
			}
		?>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label" >Nombre</label>
            <div class="col-sm-10">
              <input type="tel" class="form-control" id="material_nombre" name="material_nombre" value="<?php echo $valores['nombre'] ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Descripcion</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="material_descripcion" name="material_descripcion" value="<?php echo $valores['descripcion'] ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Cantidad total</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="material_cantidad_total" name="material_cantidad_total" value="<?php echo $valores['cantidad_total'] ?>">
            </div>
          </div>
		  
          <div class="form-group">
            <label class="col-sm-2 control-label" >Cantidad disponible</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="material_cantidad" name="material_cantidad" value="<?php echo $valores['cantidad'] ?>">
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>