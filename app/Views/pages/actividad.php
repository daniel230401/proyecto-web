<div class="mb-2 col-xs-5 col-md-7"">
            <?php
			
			$id=session()->getFlashdata('id');
			$model = new \App\Models\ActivityModel();
			$valores=$model->authenticate($id,'id');
			echo '<form action='.base_url("activity/change").'>';
            ?>
      <div class="panel panel-default">
        <div class="panel-heading">
		</br>
        <h4 class="panel-title">Información de la actividad</h4>
        </div>
		<?php
			if(session()->getFlashdata('fallo_plazas')){
				echo '<div class="alert alert-danger" role="alert">
				Las plazas actuales no pueden ser mayor que las plazas totales
				</div>';
			}
		?>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label" >Nombre</label>
            <div class="col-sm-10">
              <input type="tel" class="form-control" id="actividad_nombre" name="actividad_nombre" value="<?php echo $valores['nombre'] ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Descripcion</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="actividad_descripcion" name="actividad_descripcion" value="<?php echo $valores['descripcion'] ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" >Plazas totales</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="actividad_plazas_totales" name="actividad_plazas_totales" value="<?php echo $valores['plaza_total'] ?>">
            </div>
          </div>
		  
          <div class="form-group">
            <label class="col-sm-2 control-label" >Plazas disponibles</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="actividad_plazas_totales" name="actividad_plazas" value="<?php echo $valores['plaza'] ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" >Fecha</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" id="actividad_fecha" name="actividad_fecha" value="<?php echo $valores['fecha_hora'] ?>">
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
              <button type="submit" class="btn btn-info">Actualizar</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>