<!DOCTYPE HTML>
<head>
</head>

<body>
</br>
<h1>Listado de actividades</h1>
</br>
<?php

if(session('rol')=="ADMIN"){
	echo '<a href="new_activity">';
		echo '<input type="button" class="btn btn-success" value="Añadir actividad">';
	echo '</a>';	
}

echo "<table class='table table-sm table-dark'>";
echo "<tr>";
echo "  <th  scope='col'> Nombre  </th>";
echo "  <th scope='col'> Descripción </th>";
echo "	<th scope='col'> Fecha </th>";
echo "  <th scope='col'> Plazas </th>";
echo "  <th scope='col'></th>";
if(sizeof($activities)>0){
	foreach($activities as $row){
		echo "<tr>";
		$actividad = $row->id;
			echo '<td>' .$row->nombre. '</td>';
			echo "<td>" .$row->descripcion. "</td>";
			echo "<td>" .$row->fecha_hora. "</td>";
			if(null!=session('nick')){ //Sesión iniciada 
				echo "<td>";
					if($row->plaza!="0"){
					echo '<form action='.base_url("inscripcion/inscripcion").'>'; //INVENT
						echo '<input type="text" style="display:none" name="id" value="'.$actividad.'">';
						echo '<button class="btn btn-outline-info" type="submit">'.$row->plaza.'/'.$row->plaza_total.'</button>';//END INVENT
					}else{
						echo "<p class='text-danger'>AGOTADO</p>";
					}
				echo "</td>";
					echo '</form>';
					if(session('rol')=="ADMIN"){
						echo "<td>";
						
							echo '<form action='.base_url("activity/borrar").'>';//INVENT
								echo '<input type="text" style="display:none" name="id" value="'.$actividad.'">';
								echo '<button class="btn btn-danger" type="submit">Borrar</button>';//END INVENT
							echo "</form>";
						
							echo '<form action='.base_url("activity/change_activity").'>';
								echo '<input type="text" style="display:none" name="id" value="'.$actividad.'">';
								echo '<button class="btn btn-primary" type="submit">Modificar</button>';
							echo "</form>";
							

						echo "</td>";
					}
				
			}	
		echo "</tr>";
		echo "<br/>";
	}
}else{
	echo "No existen actividades en la base de datos.";
}
echo "</tr>";
echo "</table>";

?>
