



  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
      <!-- /.col-lg-4 -->
      <!-- /.col-lg-4 -->
      <!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div>
      <div class="">
        <h2 class="featurette-heading">Hola, <span class="text-muted">bienvenido a la ludoteca</span></h2>
        <p class="lead">Un lugar de juegos y sueños</p>
      </div>
      
    </div>

    <hr class="featurette-divider">

    <div class="">
      <div class="">
        <h2 class="featurette-heading">Quiénes somos<span class="text-muted">,</span></h2>
        <p class="lead">Somos un conjunto de amigos que hemos formado una asociación donde poder conocer gente con los mismo intereses de juegos tanto de mesa como de multiplataforma y esperamos que quieras conocernos </p>
      </div>
      
    </div>

    

    

    <hr class="featurette-divider">

    <div class="">
        <h2 class="featurette-heading">Qué ofrecemos<span class="text-muted">,</span></h2>
        <p class="lead">Ofrecemos una web donde puedes gestionar las actividades y materiales que quieras utilizar en nuestra ludoteca así como actos sociales donde podrás conocer gente nueva con los mismos intereses</p>
      </div>

  </div>