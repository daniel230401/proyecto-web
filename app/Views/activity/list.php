<h1>Lista de mis Actividades</h1>
<?php

if(sizeof($inscripciones)>0)
{
	$model = new \App\Models\ActivityModel();
	echo '<table class="table table-striped">';
		echo '<tr>';
		echo '<th>Nombre</th>';
		echo '<th>Fecha</th>';
		echo '<th></th>';
		echo '</tr>';
	foreach($inscripciones as $row)
	{
		echo '<tr>';
		$data=$model->authenticate($row->actividad,'id');
		echo '<td>' .$data['nombre'].'</td>';
		echo '<td>' .$data['fecha_hora'].'</td>';
		echo '<td>';
			echo '<form action='.base_url("inscripcion/borrar").'>';
								echo '<input type="text" style="display:none" name="id" value="'.$data['id'].'">';
								echo '<button class="btn btn-danger" type="submit">Anular</button>';
			echo '</form>';
		echo '</td>';
		echo '</tr>';
	}
	echo '</table>';
}else
{
	
	echo "No te has registrado en nignuna actividad";
}
?>
