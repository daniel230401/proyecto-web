<?php
	namespace App\Filters;
	use CodeIgniter\HTTP\RequestInterface;
	use CodeIgniter\HTTP\ResponseInterface;
	use CodeIgniter\Filters\FilterInterface;
	
	class AdminFilter implements FilterInterface
	{
		public function before(RequestInterface $request, $arguments = null)
		{
			if (!session('logged_in'))
			{
				// Podemos redireccionar donde sea conveniente
				return redirect()->to(site_url('user/auth'));
			} else if (session('rol')!= "ADMIN")
				{
					return redirect()->to(site_url('user/admin_403'));
				}
		}
		public function after(RequestInterface $request, ResponseInterface $response,
			$arguments = null)
		{
			// Do something here
		}
	}