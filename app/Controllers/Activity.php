<?php
namespace App\Controllers;
include 'General.php';
use CodeIgniter\Controller;

class Activity extends Controller{
	
	public function show(){
		
		helper(['form', 'url']);

		$model = new \App\Models\ActivityModel();
		$data['activities'] = $model->findAll();
		
		echo view(head()); 
		echo view('pages/activities',$data);
		echo view('templates/footer');
	}
	
	public function borrar(){
		if(session('rol')=="ADMIN"){
			$id=$this->request->getVar("id");
			$modelActivity = new \App\Models\ActivityModel();
			$modelInsc=new \App\Models\InscripcionModel();
			$modelInsc->where('id', $id)->delete();
			$modelActivity->delete($id);
			return redirect()->to(base_url('activity/show'));
		}else{
			return redirect()->to(base_url('pages/admin_403'));
		}

	}
	
	
	public function new_activity(){ #Función para acceso del formulario de nueva actividad
		helper('form','url');
		$data = [];
        echo view(head());
        echo view('pages/add_activity',$data);
        echo view('templates/footer');
	}
	
	public function add_activity(){ #Función para agregar una nueva actividad a la base de datos
		helper('form','url');
		$model = new \App\Models\ActivityModel();
		$validation = \Config\Services::validation();
		$session = session();
		
		$nombre = $this->request->getVar('actividad_nombre');
		$descripcion = $this->request->getVar('actividad_descripcion');
		$plazaTotal = $this->request->getVar('actividad_plaza_total');
		$plaza = $this->request->getVar('actividad_plaza_total');
		$fecha_hora = $this->request->getVar('actividad_fecha_hora');
		
		$rules = [
			"actividad_nombre"=>[
				"rules" => "required",
				"rules" => "required|max_length[25]"
			],
			"actividad_descripcion"=>[
				"rules" => "required",
				"rules" => "required|max_length[50]"
			],
			"actividad_plaza_total"=>[
				"rules" => "required"
			],
			"actividad_fecha_hora"=>[
				"rules" => "required"
			]
		];
		if($this->validate($rules)){
			$data = [
				"nombre"=>$nombre,
				"descripcion"=>$descripcion,
				"plaza_total"=>$plazaTotal,
				"plaza"=>$plaza,
				"fecha_hora"=>$fecha_hora
			];
			$model->insert($data);
			
			return redirect()->to(base_url('activity/show'));
		}
		//Errores con la validación de datos
		$resulFallos="<ul>";
		$errores = $validation->getErrors();

		foreach($errores as $key => $value)
		{
			
		  if($key=="actividad_nombre"){
			  $resulFallos.="<li>- Nombre incorrecto</li>";
		  }
		  if($key=="actividad_descripcion"){
			  $resulFallos.="<li>Descripción incorrecta</li>";
		  }
		  if($key=="actividad_plaza_total"){
			  $resulFallos.="<li>Es necesario asignar un número de plazas para la actividad</li>";
		  }
		  if($key=="actividad_fecha_hora"){
			  $resulFallos.="<li>Es necesario asignar una fecha y hora para la actividad</li>";
		  }
		}
		$resulFallos.="</ul>";
		$session->setFlashdata('fallos', $resulFallos);
		return redirect()->to(base_url('activity/new_activity'));
	}
	
	
	public function en_obras(){
		echo view(head());
        echo view('templates/construccion');
        echo view('templates/footer');
	}
	
	public function change_activity(){//lleva a la pagina de cambiar actividad
		helper(['form', 'url']);
		if(session('rol')=="ADMIN"){
			$id = $this->request->getVar("id");
			$session = session();
			$session->setFlashdata('id', $id);
			echo view(head());
			echo view('pages/actividad');
			echo view('templates/footer');
		}else{
			return redirect()->to(base_url('pages/admin_403'));
		}
	}
	
	public function change(){//update de actividad
		helper(['form']);
		$session=session();
		$model = new \App\Models\ActivityModel();
		//recogemos los datos
		$id=$session->getFlashdata('id');
		$nombre=$this->request->getVar('actividad_nombre');
		$descripcion=$this->request->getVar('actividad_descripcion');
		$plazas_totales=$this->request->getVar('actividad_plazas_totales');
		$plazas=$this->request->getVar('actividad_plazas');
		$fecha=$this->request->getVar('actividad_fecha');
		if($plazas<=$plazas_totales){
			$data = [
					"nombre"=>$nombre,
					"descripcion"=>$descripcion,
					"plaza_total"=>$plazas_totales,
					"plaza"=>$plazas,
					"fecha_hora"=>$fecha
				];
			
			$model->actualizar($id,$data);
			return redirect()->to(base_url('activity/show'));
		}else{
			$session->setFlashdata('fallo_plazas',"activado");
			return redirect()->to(base_url('activity/change_activity?id='.$id));
		}
		 

		
	}

}
?>