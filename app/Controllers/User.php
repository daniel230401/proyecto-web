<?php
namespace App\Controllers;
include 'General.php';
use CodeIgniter\Controller;

class User extends Controller
{
	
	public function home()
    {
        helper(['form', 'url']);

        echo view(head());
        echo view('pages/home');
		//redirect()->to(base_url('pages/home'));
        echo view('templates/footer');
    }
	
	
	public function login()
    {

        echo view(head());
		//redirect()->to(base_url('pages/login'));
        echo view('pages/login');
        echo view('templates/footer');
    }
	
	public function login_ok()
    {
        helper(['form', 'url']);

        echo view(head());
        echo view('pages/login_ok');
        echo view('templates/footer');
    }

    public function admin_ok()
    {
        helper(['form', 'url']);

        echo view(head());
        echo view('pages/admin_ok');
        echo view('templates/footer');
    }

    public function admin_403()
    {
        helper(['form', 'url']);

        echo view(head());
        echo view('pages/admin_403');
        echo view('templates/footer');
    }

	public function cerrarSesion(){
		//al cerrar sesión va a Home
		session()->destroy();
		return redirect()->to(base_url('pages/view'));
	}
	
	public function list()
	{
		if (session('rol') == "ADMIN"){
            $userModel = new \App\Models\UserModel();
            $data['users'] = $userModel->findAll();
            echo view(head());
            echo view('user/list',$data);
            echo view('templates/footer');
		}else{
			//return redirect()->to(site_url('user/admin_403'));
		}
	}
	
	public function register() {
        helper('form');
        echo view(head());
        echo view('pages/register');
        echo view('templates/footer');
    }

    public function add_register() {
        helper(['form']);
        $model = new \App\Models\UserModel();
        $data = [
            "nick" => $this->request->getVar('nick_register'),
            "em" => $this->request->getVar('email_register'),
            "pass" => md5($this->request->getVar('password_register')),
            "rol" => "USER",
            "ape" => $this->request->getVar('surname_register'),
            "tlf" => $this->request->getVar('phone_register')
        ];
        //$data = $this->request->getVar();

        $model->insert($data);
    }
	
	
	
	function auth()
	{
		$data = [];
		helper(['form']);
		if ($this->request->getMethod() == "post") {
			$validation = \Config\Services::validation();
			$rules = [
				"nick" => [
				"label" => "nick",
				"rules" => "required"
				//"rules" => "is_unique[user.nick]"
				//"rules" =>"required|min_length[3]|max_length[20]|valid_email|is_unique[user.email]"
				],
				"password" => [
				"label" => "Contraseña",
				"rules" => "required"

				//"rules" => "required|min_length[7]|max_length[20]"
				]
			];
		if ($this->validate($rules)) {
			// Llamada a la base de datos para validación
			$session = session();
			$model = new \App\Models\UserModel();
			$nick = $this->request->getVar('nick');
			$password = md5($this->request->getVar('password'));
			$data = $model->authenticate($nick);
			if($data){
				$pass = $data['pass'];
				
				if($password == $pass)
					$verify_pass = TRUE;
				else
					$verify_pass = FALSE;
					
				if($verify_pass){
					session()->set("nick", $data['nick']);
					session()->set("email", $data['em']);
					session()->set("rol", $data['rol']);
					session()->set("logged_in", TRUE);
					
					if($data['rol'] == "ADMIN")
						return redirect()->to(site_url('user/admin_ok'));
					else
						return redirect()->to(site_url('user/login_ok'));
				}else{
					$session->setFlashdata('msg', 'Contraseña incorrecta');
					return redirect()->to(site_url('user/auth'));
				}
			}else{
				$session->setFlashdata('msg', 'Nick no encontrado');
				return redirect()->to(site_url('user/auth'));
			}
		} else {
			$data["errors"] = $validation->getErrors();
		}
	}
		echo view(head());
		echo view('pages/login', $data);
		echo view('templates/footer');
	}
	
	public function preferencias(){
		helper('form','url');
		$data = [];
		echo view(head());
		echo view('pages/preferencias');
		echo view('templates/footer');
	}
	
	public function change_preferencias(){
		helper('form','url');
		$validation = \Config\Services::validation();
		$modelUser = new \App\Models\UserModel();
		$session = session();
		
		$usuario = session('nick');
		$old_pass = $this->request->getVar('preferencias_pass_actual');
		if($modelUser->comprobar_contrasena($usuario,$old_pass)){
			$nombre = $this->request->getVar('preferencias_nombre');
			$apellidos = $this->request->getVar('preferencias_apellidos');
			$telefono = $this->request->getVar('preferencias_telefono');
			$email = $this->request->getVar('preferencias_email');
			$pass = $this->request->getVar('preferencias_pass_new');
	
			$data = [
				"nick"=>$usuario,
				"em"=>$email,
				"pass"=>$pass,
				"nombre"=>$nombre,
				"ape"=>$apellidos,
				"tlf"=>$telefono,		
			];
			
			$modelUser->actualizar($usuario,$data);
		}
		return redirect()->to(base_url('pages/view'));
	}
	
	
	
}
?>