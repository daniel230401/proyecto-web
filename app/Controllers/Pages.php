<?php

namespace App\Controllers;
include 'General.php';
use CodeIgniter\Controller;

class Pages extends Controller
{
	public function index()
	{
		return view('welcome_message');
	}
	
	public function view($page = 'home')
	{
		if(!is_file(APPPATH.'/Views/pages/'.$page.'.php'))
		{
			//pagina no encontrada
			throw new \CodeIgniter\Exceptions\PageNotFoundException($page);	
		}
		
	$data['title'] = ucfirst($page); // datos enviados
	
	echo view(head(),$data);
	echo view('pages/'.$page,$data);
	echo view('templates/footer',$data);
	}
}

?>