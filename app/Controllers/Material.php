<?php
namespace App\Controllers;
include 'General.php';
use CodeIgniter\Controller;

class Material extends Controller{
	
	public function show(){
		
		helper(['form', 'url']);

		$model = new \App\Models\MaterialModel();
		$data['materiales'] = $model->findAll();
		
		echo view(head()); 
		echo view('pages/materiales',$data);
		echo view('templates/footer');
	}
	
	public function borrar(){
		if(session('rol')=="ADMIN"){
			$id=$this->request->getVar("id");
			$modelMaterial = new \App\Models\MaterialModel();
			$modelReserva=new \App\Models\ReservaModel();
			$modelReserva->where('id', $id)->delete();
			$modelMaterial->delete($id);
			return redirect()->to(base_url('material/show'));
		}else{
			return redirect()->to(base_url('pages/admin_403'));
		}

	}
	
	public function new_material(){ #Función para acceso del formulario de nueva actividad
		helper('form','url');
		$data = [];
        echo view(head());
        echo view('pages/add_material',$data);
        echo view('templates/footer');
	}
	
	public function add_material(){ #Función para agregar un nuevo material a la base de datos
		helper('form','url');
		$model = new \App\Models\MaterialModel();
		$validation = \Config\Services::validation();
		$session = session();
		
		$nombre = $this->request->getVar('material_nombre');
		$descripcion = $this->request->getVar('material_descripcion');
		$cantidadTotal = $this->request->getVar('cantidad_total');
		$cantidad = $this->request->getVar('cantidad_total');
		
		$rules = [
			"material_nombre"=>[
				"rules" => "required",
				"rules" => "required|max_length[25]"
			],
			"material_descripcion"=>[
				"rules" => "required",
				"rules" => "required|max_length[50]"
			],
			"cantidad_total"=>[
				"rules" => "required"
			]
		];
		if($this->validate($rules)){
			$data = [
				"nombre"=>$nombre,
				"descripcion"=>$descripcion,
				"cantidad_total"=>$cantidadTotal,
				"cantidad"=>$cantidad
			];
			$model->insert($data);
			
			return redirect()->to(base_url('material/show'));
		}
		//Errores con la validación de datos
		$resulFallos="<ul>";
		$errores = $validation->getErrors();

		foreach($errores as $key => $value)
		{
			
		  if($key=="material_nombre"){
			  $resulFallos.="<li>- Nombre incorrecto</li>";
		  }
		  if($key=="material_descripcion"){
			  $resulFallos.="<li>Descripción incorrecta</li>";
		  }
		  if($key=="cantidad_total"){
			  $resulFallos.="<li>Es necesario asignar un número de plazas para la actividad</li>";
		  }
		}
		$resulFallos.="</ul>";
		$session->setFlashdata('fallos', $resulFallos);
		return redirect()->to(base_url('material/new_material'));
	}
	
	
	public function en_obras(){
		echo view(head());
        echo view('templates/construccion');
        echo view('templates/footer');
	}
	
	
	public function change_material(){
		helper(['form', 'url']);
		if(session('rol')=="ADMIN"){
			$id = $this->request->getVar("id");
			$session = session();
			$session->setFlashdata('id', $id);
			echo view(head());
			echo view('pages/material');
			echo view('templates/footer');
		}else{
			return redirect()->to(base_url('pages/admin_403'));
		}
	}
	
	
	public function change(){
		helper(['form']);
		$session=session();
		$model = new \App\Models\MaterialModel();
		//recogemos los datos
		$id=$session->getFlashdata('id');
		$nombre=$this->request->getVar('material_nombre');
		$descripcion=$this->request->getVar('material_descripcion');
		$cantidad_total=$this->request->getVar('material_cantidad_total');
		$cantidad=$this->request->getVar('material_cantidad');
		if($cantidad<=$cantidad_total){
			$data = [
					"nombre"=>$nombre,
					"descripcion"=>$descripcion,
					"cantidad_total"=>$cantidad_total,
					"cantidad"=>$cantidad
				];
			
			$model->actualizar($id,$data);
			return redirect()->to(base_url('material/show'));
		}else{
			$session->setFlashdata('fallo_cantidad',"activado");
			return redirect()->to(base_url('material/change_material?id='.$id));
		}
		 

		
	}
}
?>