<?php
namespace App\Controllers;
include 'General.php';
use CodeIgniter\Controller;

class Inscripcion extends Controller{


	public function inscripcion(){ #Función para realizar una inscripcion de un usuario en una actividad
		helper(['form','url']);
		
		$modelInscripcion = new \App\Models\InscripcionModel();
		$modelActividad = new \App\Models\ActivityModel();
		$usuario = session('nick');
		$actividad = $this->request->getVar("id");
		$dataActividad= $modelActividad->authenticate($actividad, "id");
		if(!$modelInscripcion->authenticate($actividad,$usuario)){
			$data = [
				"usuario"=>$usuario,
				"actividad"=>$actividad
			];
			$modelInscripcion->insert($data);
			
			$plaza=intval($dataActividad['plaza']);
			$plaza--;
			$modelActividad->actualizar($actividad,["plaza" =>$plaza]);
			return redirect()->to(base_url('inscripcion/inscripcion_list'));
		}else{
			echo "Fallo en la inscripción";
			return redirect()->to(base_url('activity/show'));
		}
		
	}
	
	public function inscripcion_list(){ #Función para mostrar las actividades inscritas de un usuario
		helper(['form','url']);
		$inscripcionModel = new \App\Models\InscripcionModel();
        $data['inscripciones'] = $inscripcionModel->where('usuario', session('nick'))->findAll();
        echo view(head());
        echo view('activity/list',$data);
        echo view('templates/footer');
	}
	
	public function borrar(){
		$modelInscripcion = new \App\Models\InscripcionModel();
		$modelActividad = new \App\Models\ActivityModel();
		$usuario = session('nick');
		$actividad = $this->request->getVar("id");
		$dataActividad= $modelActividad->authenticate($actividad, "id");
			
		$plaza=intval($dataActividad['plaza']);
		$plaza++;
		$modelActividad->actualizar($actividad,["plaza" =>$plaza]);
		
		
		
//	if(session('rol')=="ADMIN"){
		$id=$this->request->getVar("id");
		$modelInscripcion->where('actividad', $id)->delete();
		return redirect()->to(base_url('inscripcion/inscripcion_list'));
//	}else{
//		return redirect()->to(base_url('pages/admin_403'));
//	}

}

}
?>