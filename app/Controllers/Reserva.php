<?php
namespace App\Controllers;
include 'General.php';
use CodeIgniter\Controller;

class Reserva extends Controller{
	
	public function reserva_list(){ #Función para mostrar los materiales reservados por el usuario
		helper(['form','url']);
		$reservaModel = new \App\Models\ReservaModel();
        $data['reservas'] = $reservaModel->where('usuario', session('nick'))->findAll();
        echo view(head());
        echo view('material/list',$data);
        echo view('templates/footer');
	}
	
	public function reserva(){ #Función para realizar una reserva de un usuario en un material
		helper(['form','url']);
		
		$modelReserva = new \App\Models\ReservaModel();
		$modelMaterial = new \App\Models\MaterialModel();
		$usuario = session('nick');
		$material= $this->request->getVar("id");
		$dataMaterial= $modelMaterial->authenticate($material, "id");
		if($modelReserva->authenticate($material, "material") == null){
			$data = [
				"usuario"=>$usuario,
				"material"=>$material
			];
			$modelReserva->insert($data);
			
			$cantidad=intval($dataMaterial['cantidad']);
			$cantidad--;
			$modelMaterial->actualizar($material,["cantidad" =>$cantidad]);
			return redirect()->to(base_url('reserva/reserva_list'));
			
		}else{
			return redirect()->to(base_url('reserva/show'));
		}
		
	}
	
	public function show(){
        echo view(head());
        echo "Fallo, material repetido";
        echo view('templates/footer');
	}
	
	public function borrar(){
		$modelReserva = new \App\Models\ReservaModel();
		$modelMaterial = new \App\Models\MaterialModel();
		$usuario = session('nick');
		$material = $this->request->getVar("id");
		$dataMaterial= $modelMaterial->authenticate($material, "id");
			
		$cantidad=intval($dataMaterial['cantidad']);
		$cantidad++;
		$modelMaterial->actualizar($material,["cantidad" =>$cantidad]);

		$id=$this->request->getVar("id");
		$modelReserva->where('material', $id)->delete();
		return redirect()->to(base_url('reserva/reserva_list'));
	}
	
}
?>