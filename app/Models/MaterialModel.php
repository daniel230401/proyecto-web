<?php
namespace App\Models;
use CodeIgniter\Model;

class MaterialModel extends Model{
	
	protected $table = 'material';
	protected $primaryKey = 'id';
	protected $useAutoIncrement = true; #
	protected $returnType = 'object'; 
	protected $useSoftDeletes = false; #
	
	protected $allowedFields = ['nombre', 'descripcion', 'cantidad_total', 'cantidad']; #
	
	protected $useTiemstamps = false;
	
	##
	protected $validationRules = [];
	protected $validationMenssages = [];
	protected $skipValidation = false;
	
	public function authenticate($id,$valor)
	{
		$builder = $this->db->table('material');
		$builder->select('*'); // names of your columns
		$builder->where($valor, $id); // where clause
		$query = $builder->get();
		$row = $query->getRowArray();
		if (isset($row))
		{
			return $row;
		}
		return null;
	}
	
	public function actualizar($id,$data){
		foreach($data as $columna => $valor){
		$this->db->table('material')->whereIn('id',[$id])->set([$columna => $valor]) ->update();
			if($columna == "id"){
				session()->set("id", $valor);
			}
		}
	}
	
}

?>