<?php
namespace App\Models;
use CodeIgniter\Model;

class UserModel extends Model
{
	protected $table = 'user';
	protected $primaryKey = 'nick';
	#protected $useAutoIncrement = true; # db se encarga de ello
	protected $returnType = 'object';# 'objeto' o 'matriz'
	protected $useSoftDeletes = false;#verdadero si espera recuperar datos
	
	# Campos que se pueden configurar durante el método de guardar, insertar o actualizar
	protected $allowedFields = ['nick', 'em', 'pass', 'ape', 'tlf', 'rol'];
	
	protected $useTiemstamps = false; # sin marcas de tiempo en inserciones y actualizaciones
	
	# No use reglas de validación (por el momento ...)
	protected $validationRules = [];
	protected $validationMenssages = [];
	protected $skipValidation = false;
	
		
	public function authenticate($nick)
	{
		
		$builder = $this->db->table('user');
		$builder->select('*'); // names of your columns
		$builder->where('nick', $nick); // where clause
		$query = $builder->get();
		$row = $query->getRowArray();
		if (isset($row))
		{
			return $row;
		}
		return null;
	}
	
	public function comprobar_contrasena($nick,$pass)
	{
		
		$builder = $this->db->table('user');
		$builder->select('*'); // names of your columns
		$builder->where('nick', $nick); // where clause
		$builder->where('pass', md5($pass)); // where clause
		$query = $builder->get();
		$row = $query->getRowArray();
		if (isset($row))
		{
			return true;
		}
		return false;
	}
	
	
	public function actualizar($id,$data){
		foreach($data as $columna => $valor){
			if($valor != ""){	
				if($columna == "pass"){
					$this->db->table('user')->where('nick',[$id])->set([$columna => md5($valor)]) ->update();
				}else{
					$this->db->table('user')->where('nick',[$id])->set([$columna => $valor]) ->update();
				}
					if($columna == "nick"){
						session()->set("nick", $valor);
					}
				
			}
		}
	}
}
?>