<?php
namespace App\Models;
use CodeIgniter\Model;

class ActivityModel extends Model{
	
	protected $table = 'activity';
	protected $primaryKey = 'id';
	protected $useAutoIncrement = true; #
	protected $returnType = 'object'; 
	protected $useSoftDeletes = false; #
	
	protected $allowedFields = ['nombre', 'descripcion', 'plaza_total', 'plaza', 'fecha_hora']; #
	
	protected $useTiemstamps = false;
	
	##
	protected $validationRules = [];
	protected $validationMenssages = [];
	protected $skipValidation = false;
	
	public function authenticate($id,$valor)
	{
		$builder = $this->db->table('activity');
		$builder->select('*'); // names of your columns
		$builder->where($valor, $id); // where clause
		$query = $builder->get();
		$row = $query->getRowArray();
		if (isset($row))
		{
			return $row;
		}
		return null;
	}
	
	public function actualizar($id,$data){
		foreach($data as $columna => $valor){
		$this->db->table('activity')->whereIn('id',[$id])->set([$columna => $valor]) ->update();
			if($columna == "id"){
				session()->set("id", $valor);
			}
		}
	}
	
}

?>