<?php
namespace App\Models;
use CodeIgniter\Model;

class InscripcionModel extends Model{
	
	protected $table = 'inscripcion';
	protected $primaryKey = 'id';
	protected $useAutoIncrement = true; #
	protected $returnType = 'object'; 
	protected $useSoftDeletes = false; #
	
	protected $allowedFields = ['usuario', 'actividad']; #
	
	protected $useTiemstamps = false;
	
	##
	protected $validationRules = [];
	protected $validationMenssages = [];
	protected $skipValidation = false;
	
	public function authenticate($actividad,$usuario)
	{
		
		$builder = $this->db->table('inscripcion');
		$builder->select('*'); // names of your columns
		$builder->where('actividad', $actividad); // where clause
		$builder->where('usuario', $usuario); // where clause
		$query = $builder->get();
		$row = $query->getRowArray();
		//echo "$row";
		if (isset($row))
		{
			return $row;
		}
		return null;
	}
	
}

?>