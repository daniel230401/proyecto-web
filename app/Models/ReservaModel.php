<?php
namespace App\Models;
use CodeIgniter\Model;

class ReservaModel extends Model{
	
	protected $table = 'reserva';
	protected $primaryKey = 'id';
	protected $useAutoIncrement = true; #
	protected $returnType = 'object'; 
	protected $useSoftDeletes = false; #
	
	protected $allowedFields = ['usuario', 'material']; #
	
	protected $useTiemstamps = false;
	
	##
	protected $validationRules = [];
	protected $validationMenssages = [];
	protected $skipValidation = false;
	
	public function authenticate($id,$valor)
	{
		
		$builder = $this->db->table('reserva');
		$builder->select('*'); // names of your columns
		$builder->where($valor, $id); // where clause
		$query = $builder->get();
		$row = $query->getRowArray();
		if (isset($row))
		{
			return $row;
		}
		return null;
	}
	
}

?>