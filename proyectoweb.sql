-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-05-2021 a las 13:05:00
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectoweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity`
--

CREATE TABLE `activity` (
  `id` int(10) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `plaza_total` int(2) NOT NULL,
  `plaza` int(2) DEFAULT NULL,
  `fecha_hora` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `activity`
--

INSERT INTO `activity` (`id`, `nombre`, `descripcion`, `plaza_total`, `plaza`, `fecha_hora`) VALUES
(2, 'Torneo Jungle Speed', 'Torneo para darnos a conocer a nuevos miembros.', 200, 198, '2021-07-27'),
(3, 'Torneo Tekken', 'Tekken 7', 10, 7, '2021-07-21'),
(10, 'Torneo LoL', 'Torneo para 10 equipos', 10, 9, '2021-07-23'),
(12, 'Iniciación D&D', 'Nueva aventura', 8, 8, '2021-07-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `id` int(10) NOT NULL,
  `usuario` varchar(80) NOT NULL,
  `actividad` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inscripcion`
--

INSERT INTO `inscripcion` (`id`, `usuario`, `actividad`) VALUES
(19, 'Emirun', 10),
(20, 'Emirun', 2),
(21, 'Adri', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `id` int(3) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `descripcion` varchar(80) NOT NULL,
  `cantidad_total` int(2) NOT NULL,
  `cantidad` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id`, `nombre`, `descripcion`, `cantidad_total`, `cantidad`) VALUES
(2, 'Jungle Speed', 'Juego completo', 2, 2),
(3, 'Manuales D&D', 'Manueles de jugador y manual Master', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `id` int(3) NOT NULL,
  `material` int(3) NOT NULL,
  `usuario` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`id`, `material`, `usuario`) VALUES
(2, 3, 'Adri');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `nick` varchar(80) NOT NULL,
  `em` varchar(80) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `ape` varchar(80) DEFAULT NULL,
  `tlf` int(9) NOT NULL,
  `rol` varchar(5) NOT NULL COMMENT 'Valores: ADMIN or USER'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`nick`, `em`, `pass`, `nombre`, `ape`, `tlf`, `rol`) VALUES
('Adri', 'adri_admin@gmail.com', '6e60a28384bc05fa5b33cc579d040c56', '', 'Amaro', 953258741, 'ADMIN'),
('Emirun', 'emirun_admin@gmail.com', '5b56707735bed7117162b252685a19a1', '', NULL, 953632658, 'ADMIN'),
('Roberto', 'rigoberto@gmail.com', '656cbd4740f8401e307906140a0f83d3', '', NULL, 953478111, 'USER');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario` (`usuario`),
  ADD KEY `actividad` (`actividad`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`),
  ADD KEY `material` (`material`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`nick`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `inscripcion_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `user` (`nick`),
  ADD CONSTRAINT `inscripcion_ibfk_2` FOREIGN KEY (`actividad`) REFERENCES `activity` (`id`);

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reserva_ibfk_1` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `reserva_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `user` (`nick`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
